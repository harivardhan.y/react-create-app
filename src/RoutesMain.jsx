import React from 'react';
import { Route, Routes } from 'react-router-dom';
import SidebarItem2 from './Components/SidebarItem2';
import SidebarItem1 from './Components/SidebarItem1';
import SidebarItem5 from './Components/SidebarItem5';
import SidebarItem4 from './Components/SidebarItem4';
import SidebarItem3 from './Components/SidebarItem3';

const RoutesMain = () => {
    return (
        <div className='main_body'>
            <Routes>
                <Route path="/sidebaritem1" element={<SidebarItem1 />} />
                <Route path="/sidebaritem2" element={<SidebarItem2 />} />
                <Route path="/sidebaritem3" element={<SidebarItem3 />} />
                <Route path="/sidebaritem4" element={<SidebarItem4 />} />
                <Route path="/sidebaritem5" element={<SidebarItem5 />} />
            </Routes>
        </div>
    );
};

export default RoutesMain;