import { Provider } from 'react-redux';
import './App.css';
import SideBar from './Components/SideBar';
import NavigationBar from './ReusableComponents/NavigationBar';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { store } from './Redux/store';

import RoutesMain from './RoutesMain';
import LoginPage from './LoginPage';

function App() {
  return (
  
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/*" element={
              <div>
                <NavigationBar />
                <SideBar />
                <RoutesMain/>
              </div>
            }
          />
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
