import React from 'react';
import SideContent from '../ReusableComponents/SideContent';

const SideBar = () => {
    
    return (
         
        <aside className='p-2 py-1 side_content '  >
        <SideContent/>
        </aside>  
        
        
    );
};

export default SideBar;