import { configureStore } from '@reduxjs/toolkit';
import rootReducer from '../Redux/features/slices';

export const store = configureStore({
  reducer: rootReducer,
});
