import React from 'react';

const NavigationBar = () => {
    return (
        <nav className="navbar sticky-top bg-light navbar_main">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">Sticky top</a>
        </div>
      </nav>
    );
};

export default NavigationBar;