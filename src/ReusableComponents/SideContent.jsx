import React from 'react';
import { NavLink } from 'react-router-dom';

const SideContent = () => {
    const iconsList = [
        {icon: <i className="fa-solid fa-user"></i>, url:"/sidebaritem1",  description:"sidebar-item-1"  },
        {icon: <i className="fa-brands fa-slack"></i>, url:"/sidebaritem2",  description:"sidebar-item-2"  },
        {icon: <i className="fa-brands fa-figma"></i>, url:"/sidebaritem3",  description:"sidebar-item-3"  },
        {icon: <i className="fa-solid fa-list"></i>, url:"/sidebaritem4",  description:"sidebar-item-4"  },
        {icon: <i className="fa-solid fa-snowflake"></i>, url:"/sidebaritem5",  description:"sidebar-item-5"  },
        {icon: <i className="fa-solid fa-snowflake"></i>, url:"/sidebaritem5",  description:"sidebar-item-5"  },
        {icon: <i className="fa-solid fa-snowflake"></i>, url:"/sidebaritem5",  description:"sidebar-item-5"  },
        {icon: <i className="fa-solid fa-snowflake"></i>, url:"/sidebaritem5",  description:"sidebar-item-5"  },
    
    ]
    return (
        <div className='sidebar_main  '>
           {iconsList?.map(i =>{
    return(
        <ul className='ps-2 mb-0 first_child'>
        <NavLink to={i.url} className='text-decoration-none text-dark'>
        <li className='ActionListContent '>
        <span className='ActionListItem-visual  ActionListItem-visual--leading' >{i.icon}</span>
        <span className=' ActionListItem-label'>{i.description}</span>
        </li>  
        </NavLink>
        </ul>
    )
})}
        </div>
    );
};

export default SideContent;